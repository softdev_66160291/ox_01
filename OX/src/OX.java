import java.util.Scanner;

public class OX {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        OX game = new OX();

        System.out.println("Welcome to XO Game");
        while (game.isFull() && game.checkWin()) {
            game.printTable();
            game.printTurn();
            System.out.print("Enter your row(1-3) : ");
            int Player_row = scanner.nextInt() - 1;
            System.out.print("Enter your column(1-3) : ");
            int Player_col = scanner.nextInt() - 1;
            System.out.println("------------------------------");
            if (game.Playable(Player_row, Player_col)) {
                System.out.println("Please try again");
                continue;
            }
            else {
                game.Play(Player_row, Player_col);
            }
        }
        if (!game.isFull()) {
            System.out.println("Draw!");
        }
        else if (!game.checkWin()) {
            game.printTable();
            game.printWin();
        }
        System.out.println("------------------------------");
    }
    int[][] Table = {
        { 2, 2, 2,},
        { 2, 2, 2,},
        { 2, 2, 2,}
    }; // 0=X , 1=O , 2=Non
    int Turn = 0; // Player Turn
    public OX(){}
    public void printTurn() {
        if (Turn % 2 == 0) {
            System.out.println("X Turn");
        }
        else {
            System.out.println("O Turn");
        }
    }
    public void Play(int row, int col) {
        int Player = Turn % 2;
        Table[row][col] = Player;
        Turn++;
    }
    public boolean Playable(int row, int col) {
        boolean occupy = false;
        if (row > 2 || col > 2) {
            System.out.println("Out of space!");
            occupy = true;
        }
        else if (Table[row][col] != 2) {
            System.out.println("This space is already occupied!");
            occupy = true;
        }
        return occupy;
    }
    public void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[i][j] == 0) {
                    System.out.print("X ");
                }
                else if (Table[i][j] == 1) {
                    System.out.print("O ");
                }
                else {
                    System.out.print("- ");
                }
            }
            System.out.println();
        }
    }
    public boolean isFull() {
        boolean Full = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[i][j] == 2) {
                    Full = false;
                }
            }
        }
        return !Full;
    }
    public boolean checkWin(){
        boolean row_Win = false;
        boolean col_Win = false;
        boolean dia_Win = false; 
        for  (int i = 0; i < 3; i++) {
            if (Table[i][0] == Table[i][1] && Table[i][1] == Table[i][2] && Table[i][0] != 2) {
                row_Win = true;
            } // Check row
            if (Table[0][i] == Table[1][i] && Table[1][i] == Table[2][i] && Table[0][i] != 2) {
                col_Win = true;
            } // Check col
        }
        if (Table[0][0] == Table[1][1] && Table[1][1] == Table[2][2] && Table[0][0] != 2){
            dia_Win = true;
        }
        else if (Table[0][2] == Table[1][1] && Table[1][1] == Table[2][0] && Table[0][2] != 2){
            dia_Win = true;
        } // Check Diagnal
        boolean Win = row_Win || col_Win || dia_Win;
        return !Win;
        }
    public void printWin() {
        String Winner = "";
        if ((Turn+1) % 2 == 0) {
            Winner = "X";
        }
        else {
            Winner = "O";
        }
        System.out.println("Player "+Winner+" Win !!!");
    }
}